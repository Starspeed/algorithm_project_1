n = int(input('How many rows do you want? '))

def helpme(n):

    num = 1
    row = 1
    col = 1
    string = " "
    rowTotal = 0
    for i in range(2, n+2):
        for j in range(1, i):
            string = string + " " + str(num)
            rowTotal += num
            num += 1
            col += 1
        string += ' ' + 'Row total = ' + str(rowTotal) + " Equation = " + str(j*(j**2 + 1)//2)
        print(string)
        string = " "
        rowTotal = 0
print(helpme(n))

